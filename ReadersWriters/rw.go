package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var waitGroup sync.WaitGroup

func Reader(ch chan int, wg *sync.WaitGroup, id int) {
	waitGroup.Done()
	fmt.Printf("Reader %d wants to read\n", id)
	token := <-ch
	wg.Add(1)
	fmt.Printf("Reader %d starts reading\n", id)
	ch <- token
	//simulate read()
	time.Sleep(time.Duration(rand.Int()%5) * time.Second)
	fmt.Printf("Reader %d ends reading\n", id)
	wg.Done()
}

func Writer(ch chan int, wg *sync.WaitGroup, id int) {
	waitGroup.Done()
	fmt.Printf("Writer %d wants to write.\n", id)
	token := <-ch
	wg.Wait()
	fmt.Printf("Writer %d starts writing.\n", id)
	//simulate write
	time.Sleep(time.Duration(rand.Int()%10) * time.Second)
	fmt.Printf("Writer %d ends writing.\n", id)
	ch <- token
}

func main() {
	wg := sync.WaitGroup{}
	ch := make(chan int)
	waitGroup.Add(10)
	for i := 1; i <= 10; i++ {
		reader := rand.Int() % 2
		if reader > 0 {
			go Reader(ch, &wg, i)
		} else {
			go Writer(ch, &wg, i)
		}
	}
	ch <- 1
	waitGroup.Wait() //ensures that all goroutines started and wait for token
	<-ch // get the last token
}
