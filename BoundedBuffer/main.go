package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func Producer(ch chan int) {
	for i := 0; i < 20; i++ {
		val := rand.Int() % 100
		ch <- val
		fmt.Printf("Producer put an item (value = %d) to channel.\n", val)
		time.Sleep(time.Second * 2)
	}
	close(ch)
}

func Consumer(ch chan int, cid int, wg *sync.WaitGroup) {
	defer wg.Done()
	for val := range ch {
		fmt.Printf("Consumer %d received an item %d from channel.\n", cid, val)
		time.Sleep(time.Second * 5)
	}
}

func main() {
	wg := sync.WaitGroup{}
	ch := make(chan int, 2)
	wg.Add(2)
	go Producer(ch)
	go Consumer(ch, 1, &wg)
	go Consumer(ch, 2, &wg)
	wg.Wait()
}
