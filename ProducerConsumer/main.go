package main

import (
	"fmt"
	"math/rand"
	"time"
)

func producer(ch chan int, stopCh chan interface{}) {
	val := rand.Int() % 100
	for {
		select {
		case ch <- val:
			fmt.Printf("Producer sent: %d\n", val)
			val = rand.Int() % 100
			time.Sleep(time.Second)
		case <-stopCh:
			fmt.Println("Stop producer.")
			close(ch)
			stopCh <- 0
			break
		}
	}
}

func consumer(ch chan int, cid int) {
	for val := range ch {
		fmt.Printf("Consumer %d received: %d\n", cid, val)
		time.Sleep(time.Second * 2)
	}
}

func main() {
	var ch chan int
	var stopCh chan interface{}
	ch = make(chan int)
	stopCh = make(chan interface{})
	go producer(ch, stopCh)
	go consumer(ch, 1)
	go consumer(ch, 2)
	time.Sleep(time.Second * 5)
	stopCh <- 0
	<-stopCh
}
